from datetime import datetime
from typing import ClassVar

from pydantic import BaseModel, EmailStr, Field, HttpUrl


class Price(BaseModel):
    amount: float
    currency: str


def dt_now():
    return datetime.utcnow().replace(microsecond=0)


class DbBaseModel(BaseModel):
    query_identifier: ClassVar = ()
    created: datetime = Field(default_factory=dt_now)
    updated: datetime | None = None

    @property
    def query_filter(self):
        return {key: self.model_dump(mode="json")[key] for key in self.query_identifier}

    @property
    def exclude_for_update(self):
        return {"created"}

    def dump_for_update(self):
        self.updated = dt_now()
        return self.model_dump(mode="json", exclude=self.exclude_for_update)


class Band(BaseModel):
    name: str
    countries: tuple[str, ...] | None = None
    website: HttpUrl | None = None
    genres: tuple[str, ...] | None = None


class Location(DbBaseModel):
    query_identifier: ClassVar = ("name", "street")

    name: str
    street: str
    street_number: str
    postal_code: int
    city: str
    phone: str | None = None
    email: EmailStr | None = None
    homepage_url: HttpUrl | None = None


class ConcertModel(DbBaseModel):
    """Basemodel for concerts"""

    query_identifier: ClassVar = ("start", "location_name")
    source_url: HttpUrl
    location_name: str
    bands: tuple[Band, ...]

    start: datetime
    timezone: str = "Europe/Zurich"
    doors: datetime | None = None
    end: datetime | None = None
    price: Price | None = None
    ticket_seller_url: HttpUrl | None = None
    description: str | None = None
    video_urls: tuple[HttpUrl] | None = None
    sold_out: bool = False
