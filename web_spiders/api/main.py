from fastapi import APIRouter, FastAPI
from fastapi.responses import HTMLResponse, RedirectResponse

from web_spiders.models import ConcertModel
from web_spiders.pipelines import BandEventStorePipeline, get_db


class SpiderFastApi(FastAPI):
    def configure_homepage_redirect(self):
        @self.get(
            "/",
            name="home",
            tags=["homepage"],
            response_class=HTMLResponse,
            include_in_schema=False,
        )
        def homepage():
            return RedirectResponse("/docs")


api_router = APIRouter(prefix="/api")


@api_router.get("/events", response_model=list[ConcertModel])
async def events(loc: list[str] | None = None):
    db = get_db()
    coll = getattr(db, BandEventStorePipeline.collection_name)
    query = {}
    return [ConcertModel(**item) for item in coll.find(query)]


def create_app() -> SpiderFastApi:
    app = SpiderFastApi(title="Was lauft!")
    app.configure_homepage_redirect()
    app.include_router(api_router)
    return app
