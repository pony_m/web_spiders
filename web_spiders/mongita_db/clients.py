from functools import lru_cache
from pathlib import Path

from mongita import MongitaClientDisk

from web_spiders.storage_settings import MongitaSettings


@lru_cache
def get_client(path: Path | None = None) -> MongitaClientDisk:
    path = path or MongitaSettings().mongita_path
    c = MongitaClientDisk(host=path)
    return c
