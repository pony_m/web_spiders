# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import logging
from functools import cached_property, lru_cache

import pydantic
from scrapy.exceptions import DropItem

from web_spiders.models import ConcertModel, Location
from web_spiders.mongita_db.clients import get_client

logger = logging.getLogger(__name__)
db_name = "events"


@lru_cache
def get_db():
    return getattr(get_client(), db_name)


class StorageBase:
    pydantic_model = None
    collection_name = None

    def __init__(self):
        self._client = get_client()

    @staticmethod
    def validate_model(item_data, model):
        try:
            model = model(**item_data)
        except pydantic.ValidationError as err:
            raise DropItem(f"{err}") from err
        return model

    @cached_property
    def collection(self):
        return getattr(get_db(), self.collection_name)

    def close_spider(self, spider):
        self._client.close()

    def create_or_update(
        self,
        data: dict,
        update: bool = True,
        drop_on_exists: bool = True,
    ):
        model = self.validate_model(data, self.pydantic_model)
        q = model.query_filter
        existing = self.collection.find_one(q)
        if not existing:
            model_dict = model.model_dump(mode="json")
            self.collection.insert_one(model_dict)
            logging.info(f"New {self.pydantic_model.__name__} added: {q}")
            return

        if not update:
            if not drop_on_exists:
                return
            raise DropItem(
                f"Model {self.pydantic_model.__name__} duplicate found by fields:"
                f" {', '.join(self.pydantic_model.query_identifier)}"
            )
        result = self.collection.update_one(
            {"_id": existing["_id"]},
            {"$set": model.dump_for_update()},
        )
        if result.matched_count == 1:
            logging.debug(f"Record updated: {q}")


class LocationStorePipeline(StorageBase):
    collection_name = "band_events"
    pydantic_model = Location

    def open_spider(self, spider):
        location = getattr(spider, "location", None)
        if not location:
            return
        self.create_or_update(location, update=False, drop_on_exists=False)

    def process_item(self, item: dict, spider):
        """Does not deal with any scroped data, just hands it over. The spider defines the location
        as class variable"""
        return item


class BandEventStorePipeline(StorageBase):
    collection_name = "bands"
    pydantic_model = ConcertModel

    def process_item(self, item: dict, spider):
        self.create_or_update(item, update=True)
        return item
