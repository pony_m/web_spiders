import re
from datetime import datetime

import scrapy

month_map = {
    "januar": 1,
    "februar": 2,
    "märz": 3,
    "april": 4,
    "mai": 5,
    "juni": 6,
    "juli": 7,
    "august": 8,
    "september": 9,
    "oktober": 10,
    "november": 11,
    "dezember": 12,
}

price_patt = re.compile(r"\d+\.?\d*")


class EllokalSpider(scrapy.Spider):
    name = "ellokal"
    allowed_domains = ["www.ellokal.ch"]
    start_urls = ["http://www.ellokal.ch/?lang=de&details=9"]

    location = {
        "name": "El Lokal",
        "street": "Gessner-Allee",
        "city": "Zürich",
        "street_number": "11",
        "postal_code": 8001,
        "phone": "+41 43 344 87 50",
        "homepage_url": "http://www.ellokal.ch",
    }

    def parse(self, response):
        now = datetime.utcnow()
        ticket_seller_url = "https://www.ticketino.com/de/Search/?q=el%20lokal"
        for item in response.xpath(
            "//div[@id='maincontent']//div[contains(@class, 'commingupEventsList_0') or"
            " contains(@class, 'commingupEventsList_1')]"
        ):
            band_name = item.xpath("div[5]//a/@title").get()
            band_countries_str = (
                item.xpath("div[5]//a//span/text()").get().lstrip("(").rstrip(")")
            )

            day_num = item.xpath("div[2]/text()").get().strip().rstrip(".")
            hour, minute = item.xpath("div[4]/text()").get().split("Uhr")
            month_num = month_map.get(item.xpath("div[3]/text()").get().lower())
            price_str = item.xpath("div[6]/text()").get().strip(".-")
            price = float(price_patt.search(price_str).group(0))
            band = {"name": band_name, "countries": band_countries_str.split("/")}
            # tz is not preserved when writing to the db
            local_start = datetime(
                month=int(month_num),
                day=int(day_num),
                hour=int(hour),
                minute=int(minute),
                year=now.year,
            )

            yield {
                "start": local_start,
                "price": {"amount": price, "currency": "CHF"},
                "ticket_seller_url": ticket_seller_url,
                "bands": [band],
                "source_url": response.url,
                "location_name": self.location["name"],
            }
