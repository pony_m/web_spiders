import re
from datetime import datetime

import scrapy

from web_spiders.patterns import extract_date, extract_time

support_rgx = re.compile(r"Support:\n?\s+")
genre_cls_patt = re.compile(r"genre-")


class ExilclubSpider(scrapy.Spider):
    name = "exilclub"
    allowed_domains = ["exil.club"]
    start_urls = ["https://exil.club/events/"]

    location = {
        "name": "EXIL Club",
        "street": "Hardstrasse",
        "city": "Zürich",
        "street_number": "245",
        "postal_code": 8005,
        "email": "INFO@EXIL.CL",
        "phone": "+41 (0)43 366 86 84",
        "homepage_url": "https://exil.club",
    }

    def extract_genre(self, class_names: list[str]):
        for cls_n in class_names:
            if cls_n.startswith("genre-"):
                yield genre_cls_patt.sub("", cls_n)

    def extract_support(self, text: str):
        name = support_rgx.sub("", text)
        return name.strip()

    def clean_description(self, text: str):
        return text

    def extract_price(self, text: str | None):
        if not text:
            return
        m = re.search(r"\d{1,3}", text)
        if not m:
            return
        amount = float(m.group(0))
        return {"amount": amount, "currency": "CHF"}

    def parse_event_page(self, response):
        classes = response.css("a.active").xpath("@class").get().split(" ")
        bands = [
            {
                "name": response.css("div.topbar h2").xpath("text()").get(),
                "genres": tuple(self.extract_genre(classes)),
            }
        ]

        support_act = response.css("div.support").xpath("text()").get()
        if support_act:
            bands.append({"name": self.extract_support(support_act)})

        price_str = response.css("div.price").xpath("text()").get()

        sold_out = False
        if price_str:
            price_str = price_str.rstrip()
            if "sold out" in price_str.lower():
                sold_out = True
        ticket_url = response.css("div.getTickets").xpath("a/@href").get()
        date_str = response.css("a.active").css("div.date").xpath("text()").get()
        day, month, year_short = extract_date(date_str)

        doors_date = None
        doors = response.css("div.door").xpath("text()").get()
        if doors:
            h, m = extract_time(doors)
            doors_date = datetime(
                year=int(f"20{year_short}"), month=month, day=day, hour=h, minute=m
            )
        start_str = response.css("div.start").xpath("text()").get()
        start_hour, start_min = extract_time(start_str)
        local_start = datetime(
            year=int(f"20{year_short}"),
            month=month,
            day=day,
            hour=start_hour,
            minute=start_min,
        )
        desc = "\n".join(
            self.clean_description(d)
            for d in response.css("div.block-text").xpath("//p/text()").getall()
        )
        video_url = response.css("div.video-wrapper").xpath("iframe/@src").get()

        yield {
            "start": local_start,
            "price": self.extract_price(price_str),
            "ticket_seller_url": ticket_url,
            "bands": bands,
            "source_url": response.url,
            "doors": doors_date,
            "location_name": self.location["name"],
            "description": desc,
            "video_urls": [video_url] if video_url else None,
            "sold_out": sold_out,
        }

    def parse(self, response):
        for item in response.css("a.type-live"):
            # class_names = item.xpath("@class").getall()
            # genres = self.extract_genre(class_names)
            # date_str = item.css("div.date").xpath("/text()")
            # date_str = re.sub(r"\n\s+", "", date_str)
            event_url = item.xpath("@href").get()
            yield scrapy.Request(event_url, self.parse_event_page)
