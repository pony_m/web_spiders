from pathlib import Path

from confz import BaseConfig, EnvSource
from pydantic import SecretStr


class MongitaSettings(BaseConfig):
    mongita_path: Path
    CONFIG_SOURCES = [EnvSource(allow_all=True, file="env.local")]


class S3Settings(BaseConfig):
    minio_access_key: str
    minio_secret_key: SecretStr

    CONFIG_SOURCES = [EnvSource(allow_all=True, file="env.local")]
