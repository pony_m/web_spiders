import re

price_patt = re.compile(r"(\d+)(\.\d*[^-])")
time_patt = re.compile(r"(\d{2}):(\d{2})")
date_patt = re.compile(r"(\d{2})\.(\d{2})\.(\d{2})")


def extract_price(text: str, pattern=price_patt, target_group: int = 1):
    m = pattern.search(text)
    if m:
        return float(m.group(target_group))


def extract_time(text: str, pattern=time_patt) -> tuple[int, int] | None:
    m = pattern.search(text)
    if m:
        return int(m.group(1)), int(m.group(2))


def extract_date(date_str, pattern=date_patt) -> tuple[int, int, int] | None:
    m = pattern.search(date_str)
    if not m:
        raise IndexError
    return int(m.group(1)), int(m.group(2)), int(m.group(3))
