from datetime import datetime

import pytz

from web_spiders.models import ConcertModel, DbBaseModel


def test_concert_base_model():
    now = datetime.now(tz=pytz.utc)
    assert now.tzinfo
    m = ConcertModel(
        location_name="test local",
        bands=[{"name": "A Band"}],
        start=now,
        source_url="https://example.com",
        scraped=now,
    )
    assert m.start.tzinfo


def test_db_base():
    base = DbBaseModel()
    assert base.created
