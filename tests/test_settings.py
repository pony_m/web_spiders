from pathlib import Path

from web_spiders.storage_settings import MongitaSettings, S3Settings


def test_settings_minio():
    assert Path("env.local").is_file()
    s = S3Settings()
    assert s.minio_access_key


def test_settings_db():
    MongitaSettings()
