# web-spiders

Collection of spiders for scrape events and concerts

## Installation

    poetry install


## Example

Running one of the spiders returning the scraped items (python dicts):

    poetry run scrapy crawl ellokal -O ellokal.json:json

## Development

### Adding a site for concerts

You can use the scrape shell in order find the correct paths in html

    scrapy shell "https://example.com"

If the `robots.txt` allows it, continue to add a new spider:

    scrape genspider <location-name>

Your new spider should return all data fields necessary for the model `ConcertModel`. The model is validated before storing it.

### Api

Build the container:

    docker-compose build app

Starting the api using the local `.mongita` storage:

    docker-compose up api

The api is documented, just visit `localhost:8000`.
